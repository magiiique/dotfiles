#!/bin/sh

FILES=`ls -a . | grep '^\.[^.]' | grep -v "^.git$" | grep -v "^.gitignore$"`
HERE_DIR=$(readlink -f `dirname $0`)
for f in $FILES; do
    ln -s "$HERE_DIR/$f" "$HOME/$f"
done
