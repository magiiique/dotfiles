;; FIRST THING - set .emacs.d location to depend on emacs version; crufty
;; but hopefully works
(setq user-emacs-directory (format "~/.emacs.%s.d/" emacs-version))

;; straight.el setup
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; disable package.el to go with the above
(setq package-enable-at-startup nil)

;; use-package
(straight-use-package 'use-package)

;; org-mode - workaround, see here:
;; https://github.com/radian-software/straight.el#the-wrong-version-of-my-package-was-loaded
;; my attempt at a function that jumps to the timestamp
(defun org-agenda-jump-to-timestamp ()
  (interactive)
  (let* ((marker (org-get-at-bol 'org-marker))
	 (buffer (marker-buffer marker))
	 (pos (marker-position marker)))
    (unless buffer (user-error "Trying to switch to non-existent buffer"))
    ;; change to that buffer in other window - get from tab
    (switch-to-buffer-other-window buffer)
    (widen)
    (push-mark)
    (goto-char pos)
    (recenter (/ (window-height) 2))))
(use-package org
  :config
  (setq org-todo-keywords
	'((sequence "TODO" "ONGOING" "|" "READY" "DONE")))
  (global-set-key (kbd "C-c a") #'org-agenda)
  (add-to-list 'org-agenda-files "~/notes/")
  (setq org-agenda-custom-commands
	'(("x" "Agenda and task list"
	   ((agenda "")
	    (todo)))))
  (setq org-log-done t)
  (setq org-enforce-todo-dependencies t)
  (setq org-enforce-todo-checkbox-dependencies t)
  (setq org-agenda-dim-blocked-tasks t)
  )
(use-package org-agenda
  :config
  (define-key org-agenda-mode-map (kbd "j")
    'org-agenda-jump-to-timestamp)
  )

(use-package projectile
  :straight t
  :init
  ;; otherwise doesn't seem to index
  (setq projectile-indexing-method 'alien)
  (setq projectile-completion-system 'ido)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  )

(use-package solidity-mode
  :straight t
  )

;; flycheck for Python config
(use-package flycheck-pyflakes
  :straight t)
(add-to-list 'flycheck-disabled-checkers 'python-flake8)
(add-to-list 'flycheck-disabled-checkers 'python-pylint)
(setq flycheck-python-pyflakes-executable "~/bin/pyflakes")
(add-hook 'python-mode-hook 'flycheck-mode)

(use-package nim-mode
  :straight t)

;; yasnippet
(use-package yasnippet
  :straight t
  :config
  (yas-global-mode)
  )

(use-package yasnippet-snippets
  :straight t
  :config
  (yas-reload-all)
  )

;; racket
(use-package racket-mode
  :straight t
  )

;; markdown
(use-package markdown-mode
  :straight t
  )

;; YAML...
(use-package yaml-mode
  :straight t
  )

;; for testing REST endpoints
(use-package restclient
  :straight t
  )

(use-package flycheck-rust
  :straight t)

(use-package rust-mode
  :straight t
  ;; :hook (rust-mode . #'flycheck-rust-setup)
  )

(use-package expand-region
  :straight t
  :bind ("C-=" . 'er/expand-region))

(use-package dired-narrow
  :straight t
  )

;; my own keyboard remappings
(global-set-key (kbd "<home>") 'move-beginning-of-line)
(global-set-key (kbd "<end>") 'move-end-of-line)
(global-set-key (kbd "C-c r") 'replace-string)
(global-set-key (kbd "C-c d") 'kill-whole-line)

(defun clipboard-copy (region-start region-end)
  (interactive "r")
  (let ((inhibit-message t))
    (shell-command-on-region region-start region-end "pbcopy"))
  (message "Region copied to system clipboard")
  )

(global-set-key (kbd "C-c c") 'clipboard-copy)

;; misc display
(menu-bar-mode -1)
(global-set-key (kbd "M-o") 'other-window)

;; fci-mode setup if need be
(defun setup-fci-mode ()
  (let* ((elisp-dir (expand-file-name "elisp" user-emacs-directory))
	 (fci-file
	  (expand-file-name "fill-column-indicator.el" elisp-dir)))
    (make-directory elisp-dir :parents)
    (add-to-list 'load-path (expand-file-name "elisp" user-emacs-directory))

    (unless (file-exists-p fci-file)
      (url-copy-file
       "https://raw.githubusercontent.com/alpaker/fill-column-indicator/master/fill-column-indicator.el"
       fci-file)
      (require 'fill-column-indicator))))

(if (boundp 'global-display-fill-column-indicator-mode)
    (progn
      (setq display-fill-column-indicator t)
      (setq display-fill-column-indicator "|")
      (global-display-fill-column-indicator-mode))
  (progn
    (setup-fci-mode)
    (require 'fill-column-indicator)
    (add-hook 'after-change-major-mode-hook
	      (lambda ()
		(fci-mode 1)
		(set-fill-column 80)))))


;; for working with git
(global-auto-revert-mode +1)

;; scroll by 1 line only
(setq scroll-step 1
      scroll-conservatively 0
      )

(global-set-key (kbd "M-<down>") 'scroll-up-line)
(global-set-key (kbd "M-<up>") 'scroll-down-line)
(global-set-key (kbd "M-n") 'scroll-up-line)
(global-set-key (kbd "M-p") 'scroll-down-line)

;; delete trailing newline on save
(defun delete-trailing-whitespace-except-current-line ()
  (interactive)
  (let ((begin (line-beginning-position))
	(end (line-end-position)))
    (save-excursion
      (when (< (point-min) begin)
	(save-restriction
	  (narrow-to-region (point-min) (1- begin))
	  (delete-trailing-whitespace)))
      (save-restriction
	(when (> (point-max) end)
	  (narrow-to-region (1+ end) (point-max))
	  (delete-trailing-whitespace))))))

(add-hook 'before-save-hook
	  'delete-trailing-whitespace-except-current-line)
(put 'upcase-region 'disabled nil)

;; auto-fill-mode shortcut
(global-set-key (kbd "C-c q") 'auto-fill-mode)
(put 'downcase-region 'disabled nil)

;; show paren mode
(show-paren-mode 1)
(setq show-paren-delay 0)


;; remote file editing settings
(defconst mpn-file-remote-mount-points
  (mapcar (lambda (d) (directory-file-name
                       (expand-file-name d)))
          '("~/python-dev"))
  "List of locations where remote file systems have been mounted.
Each directory listed must be an absolute expanded path and must
not end with a slash.")

(push (let ((re (regexp-opt mpn-file-remote-mount-points nil)))
        (list (concat "\\`" re "\\(?:/\\|\\'\\)")
              (concat temporary-file-directory "remote")
              t))
      auto-save-file-name-transforms)

(defun mpn-file-remote-mount-p (&optional file-name)
  "Return whether FILE-NAME is under a remote mount point.
Use ‘buffer-file-name’ if FILE-NAME is not given.  List of remote
mount points is defined in ‘mpn-file-remote-mount-points’
variable."
  (when-let ((name (or file-name buffer-file-name)))
    (let ((dirs mpn-file-remote-mount-points)
          (name-len (length name))
          dir dir-len matched)
      (while (and dirs (not matched))
        (setq dir (car dirs)
              dirs (cdr dirs)
              dir-len (length dir)
              matched (and (> name-len dir-len)
                           (eq ?/ (aref name dir-len))
                           (eq t (compare-strings name 0 dir-len
                                                  dir 0 dir-len)))))
      matched)))

(defun mpn-dont-lock-remote-files ()
  "Set ‘create-lockfiles’ to nil if buffer opens a remote file.
Use ‘mpn-file-remote-mount-p’ to determine whether opened file is
remote or not.  Do nothing if ‘create-lockfiles’ is already nil."
  (and create-lockfiles
       (mpn-file-remote-mount-p)
       (setq-local create-lockfiles nil)))

(add-hook 'find-file-hook #'mpn-dont-lock-remote-files)


;; SQL formatter
;; (defun format-sql (region-start region-end)
;;   (interactive "r")
;;   (let ((temp-file (make-temp-file "sql_query"))
;; 					; (query (buffer-substring-no-properties region-start region-end))
;; 	(query-output nil)
;; 	)
;;     ;; write to the temp file
;;     (write-region region-start region-end temp-file)
;;     ;; call the command
;;     (call-process "python3" nil
;; 					; (query-output nil)
;; 		  nil
;; 		  t "-m" "sqlparse" "-k" "upper" "-i" "lower" "-r" "--indent_width" "4")
;;     ;; replace region with it (message)
;;     ))

(defun format-sql (r-start r-end)
  (interactive "r")
  (save-excursion
    (call-process-region r-start r-end "python3" t t nil
			 "-m" "sqlparse" "-k" "upper" "-i" "lower"
			 "-")
    (replace-string "real" "REAL" nil r-start r-end)
    (replace-string "TIMESTAMP" "timestamp" nil r-start r-end)
    (replace-string "OPEN" "open" nil r-start r-end)
    (replace-string "CLOSE" "close" nil r-start r-end)
    (replace-string "text" "TEXT" nil r-start r-end)
    (replace-string "integer" "INTEGER" nil r-start r-end)
    ))

(defun toggle-python-breakpoint ()
  (interactive)
  (let* ((breakpoint-str "import pdb; pdb.set_trace()")
	 (current-line (thing-at-point 'line t)))
    (if (string-match-p (regexp-quote breakpoint-str) current-line)
	;; it's a breakpoint, delete it
	(save-excursion
	  (delete-region
	   (line-beginning-position)
	   (1+ (line-end-position))))
      ;; if it's not a breakpoint, then add one at the next line
      (save-excursion
	(goto-char (line-end-position))
	(unless (string= (string-trim current-line) "")
	  (insert "\n"))
	(insert breakpoint-str)
	(indent-region (line-beginning-position) (line-end-position))))))
(global-set-key (kbd "<f5>") 'toggle-python-breakpoint)

;; commenting-dwim - if no line selected, select current line
(defun comment-dwim-empty-line (arg)
  (interactive "*P")
  (if (use-region-p)
      (comment-dwim arg)
    (save-excursion
      (set-mark (line-beginning-position))
      (goto-char (line-end-position))
      (comment-dwim arg)
      (deactivate-mark)
      )))
(global-set-key (kbd "M-;") 'comment-dwim-empty-line)

;; comment region / current line, paste underneath
(defun modify-region (reg-start reg-end prefix)
  (interactive "r\nP")
  (unless (use-region-p)
    (setq reg-start (line-beginning-position))
    (setq reg-end (line-end-position)))
  ;; if reg-end is a start of the line, decrement it by one
  (when (use-region-p)
    (save-excursion
      (goto-char reg-end)
      (if (= (current-column) 0)
	  (setq reg-end (1- reg-end)))))
  (let* ((original-content (buffer-substring-no-properties reg-start reg-end))
	 (col (current-column))
	 (last-line (save-excursion
		      (goto-char reg-end)
		      (if (= col 0)
			  (- (line-number-at-pos) 1)
			(line-number-at-pos)))))
    (save-excursion
      (unless prefix
	(comment-region reg-start reg-end))
      (goto-line last-line)
      (goto-char (line-end-position))
      (newline)
      (insert original-content))))

(global-set-key (kbd "C-c m") 'modify-region)


;; format (refill) docstring in python-mode
(defun refill-python-docstring ()
  (interactive)
  ;; ensure I'm in a string
  ;; find start of string, make sure it is followed by newline, save line
  ;; find end of string, make sure it is 'empty', save line
  ;; refill everything inbetween
  (let* ((syntax-info (syntax-ppss))
	 (is-string (nth 3 syntax-info))
	 (string-start (nth 8 syntax-info))
	 first-line
	 end-line
	 )
    (when (null is-string)
      (error "Not inside a string"))
    (save-excursion
      (goto-char string-start)
      (unless (looking-at "\\(\"\"\"\\)\\|\\('''\\)")
	(error "Docstrings should be delimited by \"\"\" or '''"))
      (forward-char 3)
      (unless (looking-at "\s-*$")
	(insert "\n")
	(setq first-line (line-number-at-pos)))
      (save-excursion
	(goto-char string-start)
	(forward-sexp)
	(forward-char -3)
	(unless (looking-at  "\\(\"\"\"\\)\\|\\('''\\)")
	  (insert "\n"))))))

      ;; (let ((string-end (save-excursion (forward-sexp) (point))))
      ;; 	(fill-region string-start string-end)))))

;; prevent position on scrolling
(setq scroll-preserve-screen-position t)

;; set a default of 80 for fill-column
(setq-default fill-column 80)

;; disable tabs for indentation
(setq-default indent-tabs-mode nil)

;; my own little function, for easy adding imports to Python files
(defun add-import (import-text)
  "Adds an import, trying to be intelligent about where it is placed in the file"
  (interactive "MImport line to add: ")
  (save-excursion
    ;; find the *last* top-level import statement, e.g. ignoring any
    ;; indented ones
    (end-of-buffer)
    (search-backward-regexp "^import ")
    (end-of-line)
    (newline)
    (insert import-text)))

(add-hook 'python-mode-hook
          (lambda () (local-set-key (kbd "C-c i") #'add-import)))

;; swap f-string / normal string
(defun toggle-fstring ()
  (interactive)
    (unless (nth 3 (syntax-ppss))
      (error "Not inside a string"))
    (let ((string-start (nth 8 (syntax-ppss))))
      (save-excursion
        ;; (goto-char (1- string-start))
        (goto-char string-start)
        (if (eq (char-before) ?f)
            (progn
              (left-char)
              (delete-char 1))
          (insert "f")
    ))))

(global-set-key (kbd "C-c f") #'toggle-fstring)

(defun my-insert-rectangle-push-lines ()
  "Yank a rectangle as if it was an ordinary kill."
  (interactive "*")
  (when (and (use-region-p) (delete-selection-mode))
    (delete-region (region-beginning) (region-end)))
  (save-restriction
    (narrow-to-region (point) (mark))
    (yank-rectangle)))

(global-set-key (kbd "C-x r C-y") #'my-insert-rectangle-push-lines)

;; kill existing Dired buffer when navigating
(setf dired-kill-when-opening-new-dired-buffer t)

;; toggle highlight of symbol at point
(defun unhighlight-symbol-at-point ()
  "Unhighlight symbol at point"
  (interactive)
  (hi-lock-unface-buffer (hi-lock-regexp-okay (find-tag-default-as-symbol-regexp))))

(defvar highlight-toggle nil)
(defun toggle-highlight-symbol-at-point ()
  ""
  (interactive)
  (if (eq this-command last-command)
      ;; apply the toggle
      (progn
        (setq highlight-toggle (not highlight-toggle))
        (if highlight-toggle
            (unhighlight-symbol-at-point)
          (highlight-symbol-at-point)))
    ;; calling for the first time; highlight
    (progn
      (highlight-symbol-at-point)
      ;; reset the toggle var
      (setq highlight-toggle nil)
      )))

(global-set-key (kbd "C-c h") #'toggle-highlight-symbol-at-point)

;;
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backup")))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )
